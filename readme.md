A simple ncurses based library that I use to make terminal things:
* [Snake](https://gitlab.com/_mobius/snake_nc)  
* [Tetris](https://gitlab.com/_mobius/terminal-tetris)

### Depenendencies  
[ncurses](https://invisible-island.net/ncurses/#downloads).

```bash
apt install libncurses5-dev libncursesw5-dev
```

### Building with Meson
[Get Meson](https://mesonbuild.com/Getting-meson.html)

Setup and build:
```bash
meson setup build --buildtype release
cd build
meson compile
```
Setup build with `build_examples` enabled:
```
meson setup build --buildtype release -Dbuild_examples=true
```
Enable `build_examples` after setup:
```
cd build
meson configure -Dbuild_examples=true
```

### Building with CMake
[Get CMake](https://cmake.org/download/)
Setup and build:
```bash
mkdir build && cd build 
cmake -DCMAKE_BUILD_TYPE=Release ..  
cmake --build .  
```
Setup build for library and examples:
```
cmake -DCMAKE_BUILD_TYPE=Release .. -DBUILD_EXAMPLES=True
```
