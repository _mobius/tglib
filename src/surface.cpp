/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "surface.h"
#include "nc_util.h"

namespace tgl
{
    /*
    ===== Surface =====
    */
    Surface::Surface( int32_t width, int32_t height, int32_t x, int32_t y, const Color_Pair &fill, uint32_t pixel_width, uint32_t pixel_height ):
            transparent{ _color_data.transparent },
            _position{ x, y }, _pixel_width{ pixel_width }, _pixel_height{ pixel_height }, _color_data{ width, height, fill }
            
    {
        _draw_regions.reserve( width * height );
        for( auto iy = 0, 
                        ey = height, 
                        ex = width;
                iy < ey;
                ++iy 
            )
        {
            for( auto ix = 0; ix < ex; ++ix )
            {
                _draw_regions.emplace_back(
                    pixel_width, pixel_height, 
                    x + ix * pixel_width,
                    y + iy * pixel_height
                );
            }
        }
    }

    Surface::Surface( int32_t width, int32_t height, int32_t x, int32_t y, uint32_t pixel_width, uint32_t pixel_height ): Surface{ width, height, x, y, Color_Pair{}, pixel_width, pixel_height }
    {}

    bool Surface::point_within( int32_t x, int32_t y ) const noexcept
    {
        return x >= 0 && x < width() && y >= 0 && y < height();
    }

    bool Surface::point_within( const Vec2 &point ) const noexcept
    {
        return point.x >= 0 && point.x < width() && point.y >= 0 && point.y < height();
    }

    std::pair<Surface::iterator, bool> Surface::pixel_at( int32_t x, int32_t y ) noexcept
    {
        if( !point_within( x, y ) ) { return { end(), false }; }

        return { get_pixel( x, y ), true };
    }

    std::pair<Surface::const_iterator, bool> Surface::pixel_at( int32_t x, int32_t y ) const noexcept
    {
        if( !point_within( x, y ) ) { return { cend(), false }; }

        return { get_pixel( x, y ), true };
    }

    std::pair<Surface::iterator, bool> Surface::pixel_at( const Vec2 &point ) noexcept
    {
        if( !point_within( point ) ) { return { end(), false }; }

        return { get_pixel( point ), true };
    }

    std::pair<Surface::const_iterator, bool> Surface::pixel_at( const Vec2 &point ) const noexcept
    {
        if( !point_within( point ) ) { return { cend(), false }; }

        return { get_pixel( point ), true };
    }

    Surface::iterator Surface::get_pixel( int32_t x, int32_t y )
    {
        assert( x >= 0 && x <= width() && "Surface_Iterator out of bounds on X" );
        assert( y >= 0 && y <= height() && "Surface_Iterator out of bounds on Y" );

        return begin() + (y * width() + x);
    }

    Surface::iterator Surface::get_pixel( const Vec2 &p )
    {
        return get_pixel( p.x, p.y );
    }

    Surface::const_iterator Surface::get_pixel( int32_t x, int32_t y ) const
    {
        assert( x >= 0 && x <= width() && "Surface_Iterator out of bounds on X" );
        assert( y >= 0 && y <= height() && "Surface_Iterator out of bounds on Y" );
        
        return cbegin() + (y * width() + x);
    }

    Surface::const_iterator Surface::get_pixel( const Vec2 &p ) const
    {
        return get_pixel( p.x, p.y );
    }

    void Surface::set_pixel( int32_t x, int32_t y, const Color_Pair &color )
    {
        auto p = get_pixel( x, y );
        _dirty_pixels.emplace_back( p );

        p = color;
    }

    void Surface::set_pixel( const Vec2 &p, const Color_Pair &color )
    {
        set_pixel( p.x, p.y, color );
    }

    void Surface::set_pixels( const Vec2 &p1, const Vec2 &p2, const Color_Pair &color )
    {
        for( auto y = p1.y; y < p2.y; ++y )
        {
            for( auto x = p1.x; x < p2.x; ++x )
            {
                set_pixel( x, y, color );
            }
        }
    }

    void Surface::set_pixels( const std::vector<Vec2> &pl, const Color_Pair &color )
    {
        for( auto &p : pl )
        {
            set_pixel( p, color );
        }
    }

    void Surface::present()
    {
        if( _redraw_all )
        {
            for( auto p : (*this) )
            {
                draw( p, { 0, 0 }, p );
                p->present();
            }
        }
        else 
        {
            for( auto &pel : _dirty_pixels )
            {
                draw( pel.region(), { 0, 0 }, pel.color() );
                pel.region().present();
            }
        }

        _dirty_pixels.clear();   
    }

    void Surface::mark_update( iterator itr )
    {
        _dirty_pixels.emplace_back( itr );
    }
}