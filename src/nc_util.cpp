/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include "nc_util.h"

namespace tgl
{
    void draw( const Region &region, const Vec2 &p, const Color_Pair &color, char chr )
    {
        Use_Color col{ color, &region };
        for( auto y = p.y, ey = region.height(); y < ey; ++y )
        {
            for( auto x = p.x, ex = region.width(); x < ex; ++x )
            {
                wmove( region, y, x );
                waddch( region, static_cast<chtype>( chr ) );
            }
        }
    }
}