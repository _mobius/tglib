/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <type_traits>
#include <ncurses.h>

#include "keyboard.h"

namespace tgl
{
    Keyboard::Key_Info& Keyboard::operator []( int key_id )
    {
        auto [itr, good] = keys.try_emplace( key_id, key_id );

        return itr->second;
    }

    // update key states
    void Keyboard::update()
    {
        for( auto &[id, k] : keys ) { k.checked = false; }

        for( int c = ERR; (c = getch()) != ERR; )
        {
            auto &k = operator[]( c );
            k.press();
            k.checked = true;
        }

        for( auto &[id, k] : keys )
        {
            if( !k.checked )
            {
                k.release();
            }
        }
    }


    bool Keyboard::pressed( int key_id ) { return operator[]( key_id ).pressed(); }
    bool Keyboard::just_pressed( int key_id ) { return operator[]( key_id ).just_pressed(); }

    bool Keyboard::released( int key_id ) { return operator[]( key_id ).released(); }
    bool Keyboard::just_released( int key_id ) { return operator[]( key_id ).just_released(); }

    bool Keyboard::is_state( int key_id, Key_State state )
    {
        return operator[]( key_id ).is_state( state );
    }

    /*
        --- Key_Info ---
    */
    Keyboard::Key_State operator |( Keyboard::Key_State state, Keyboard::Key_State_Flag flag ) noexcept
    {
        using T = std::underlying_type_t <Keyboard::Key_State>;
        using U = std::underlying_type_t <Keyboard::Key_State_Flag>;
        return static_cast<Keyboard::Key_State>( static_cast<T>( state ) | static_cast<U>( flag ) );
    }

    Keyboard::Key_State& operator |=( Keyboard::Key_State &state, Keyboard::Key_State_Flag flag ) noexcept
    {
        state = state | flag;
        return state;
    }

    bool operator &( Keyboard::Key_State state, Keyboard::Key_State_Flag flag ) noexcept
    {
        using T = std::underlying_type_t <Keyboard::Key_State>;
        using U = std::underlying_type_t <Keyboard::Key_State_Flag>;
        return static_cast<bool>( static_cast<T>( state ) & static_cast<U>( flag ) );
    }

    Keyboard::Key_Info::Key_Info( int k ) noexcept : key{ k } { }

    bool Keyboard::Key_Info::is_down() const noexcept { return pressed(); }
    bool Keyboard::Key_Info::pressed() const noexcept { return state & Key_State_Flag::down; }
    bool Keyboard::Key_Info::just_pressed() const noexcept { return state == Key_State::just_pressed; }

    bool Keyboard::Key_Info::is_up() const noexcept { return released(); }
    bool Keyboard::Key_Info::released() const noexcept { return state & Key_State_Flag::up; }
    bool Keyboard::Key_Info::just_released() const noexcept { return state == Key_State::just_released; }

    bool Keyboard::Key_Info::is_state( Key_State state ) const noexcept
    {
        switch( state )
        {
            case Key_State::pressed:
                return pressed();
            case Key_State::just_pressed:
                return just_pressed();
            case Key_State::released:
                return released();
            case Key_State::just_released:
                return just_released();
        }

        return false;
    }

    std::chrono::milliseconds Keyboard::Key_Info::state_duration() const noexcept
    {
        return std::chrono::duration_cast<std::chrono::milliseconds>( clock::now() - last_changed );
    }
    
    void Keyboard::Key_Info::step_state() noexcept
    {
        if( state & Key_State_Flag::just && clock::now() - last_changed >= Key_Info::step_wait )
        {
            state = static_cast<Key_State>( static_cast<uint8_t>( state ) & ~static_cast<uint8_t>( Key_State_Flag::just ) );
            last_changed = clock::now();
        }
    }

    void Keyboard::Key_Info::press() noexcept
    {
        _set_state( Key_State_Flag::down );
    }

    void Keyboard::Key_Info::release() noexcept
    {
        _set_state( Key_State_Flag::up );
    }

    void Keyboard::Key_Info::_set_state( Key_State_Flag direction ) noexcept
    {
        step_state();
        if( !(state & direction) )
        {
            // try to account for delay from inital press to repeat starting
            if( direction == Key_State_Flag::up && clock::now() - last_changed < Keyboard::repeat_delay ) { return; }

            state = static_cast<Key_State>( static_cast<uint8_t>( direction ) | static_cast<uint8_t>( Key_State_Flag::just ) );
            last_changed = clock::now();
        }
    }
}
