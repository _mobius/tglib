/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <cassert>
#include <map>
#include <climits>

#include "nc_color.h"
#include "nc.h"

namespace tgl
{
    using color_registry_t = std::map<Color_Pair::id_t, Color_Pair::id_t>;
    static  color_registry_t color_registry;

    static Color_Pair::id_t next_id{ 1 };

    Color_Pair::Color_Pair( Color fg, Color bg ) noexcept
    {
        assert( NCurses::is_initialized() ); // DEBUG: be sure NCurses is created before Color_Pair

        const auto color_join = static_cast<id_t>( fg ) | (static_cast<id_t>( bg ) << (sizeof( Color ) * CHAR_BIT));

        if ( auto [el, inserted] = color_registry.try_emplace( color_join, next_id ); inserted )
        {
            _id = next_id;
            ++next_id;
            const auto err = init_pair( _id, static_cast<short>( fg ), static_cast<short>( bg ) );
            assert( err == OK && "Color_Pair: init_pair failed" );
        }
        else
        {
            _id = el->second;
        }
    }

    Color_Pair& Color_Pair::operator =( const Color_Pair &c ) noexcept
    {
        _id = c._id;
        return *this;
    }

    Color_Pair& Color_Pair::operator =( Color_Pair &&c ) noexcept
    {
        _id = c._id;
        return *this;
    }

    bool operator ==( const Color_Pair &rhs, const Color_Pair &lhs ) noexcept
    {
        return rhs.id() == lhs.id();
    }

    bool operator !=( const Color_Pair &rhs, const Color_Pair &lhs ) noexcept
    {
        return rhs.id() != lhs.id();
    }
}