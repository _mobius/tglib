/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "vec.h"

namespace tgl
{
    Vec2& Vec2::operator+=( const Vec2 &v ) noexcept
    {
        x += v.x;
        y += v.y;

        return *this;
    }

    Vec2& Vec2::operator-=( const Vec2 &v ) noexcept
    {
        x -= v.x;
        y -= v.y;

        return *this;
    }

    bool operator==( const Vec2 &v1, const Vec2 &v2 ) noexcept
    {
        return v1.x == v2.x && v1.y == v2.y;
    }

    bool operator!=( const Vec2 &v1, const Vec2 &v2 ) noexcept
    {
        return !(v1 == v2);
    }

    Vec2 operator+( const Vec2 &v1, const Vec2 &v2 ) noexcept
    {
        return {
            v1.x + v2.x,
            v1.y + v2.y
        };
    }

    Vec2 operator-( const Vec2 &v1, const Vec2 &v2 ) noexcept
    {
        return {
            v1.x - v2.x,
            v1.y - v2.y
        };
    }

    Vec2 operator-( const Vec2 &v ) noexcept
    {
        return {
            -v.x,
            -v.y
        };
    }

    Vec2 operator*( const Vec2 &v, int m ) noexcept
    {
        return {
            v.x * m,
            v.y * m
        };
    }
}