/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "nc.h"
#include "region.h"
#include "vec.h"

namespace tgl
{
    Region::Region( int32_t width, int32_t height, int32_t x, int32_t y ) noexcept: _win{ newwin( static_cast<int>( height ), static_cast<int>( width ), static_cast<int>( y ), static_cast<int>( x ) ) }
    {
    }

    Region::Region( int32_t width, int32_t height ) noexcept: Region( width, height, 0, 0 ) {}

    Region::Region( const tgl::NCurses &nc, int32_t pad ) noexcept: Region( nc.width() - (pad*2), nc.height() - (pad*2), pad, pad ) {}

    Region::Region( Region &&region ) noexcept: _win{ region._win }
    {
        region._win = nullptr;
    }

    Region::~Region()
    {
        if ( _win )
        {
            delwin( _win );
        }
    }

    Region::operator WINDOW*() const noexcept { return _win; }

    int Region::present() const noexcept { return wrefresh( _win ); }

    int32_t Region::width() const noexcept
    {
        return static_cast<int32_t>( getmaxx( _win ) );
    }

    int32_t Region::height() const noexcept
    {
        return static_cast<int32_t>( getmaxy( _win ) );
    }

    void Region::modify( int32_t width, int32_t height, int32_t x, int32_t y ) noexcept
    {
        if ( _win )
        {
            delwin( _win );
            _win = nullptr;
        }

        _win = newwin( height, width, y, x );
    }

    void Region::move( const Vec2 &p ) noexcept
    {
        if ( !_win ) { return; }

        const auto w = width();
        const auto h = height();

        delwin( _win );
        _win = nullptr;

        _win = newwin( h, w, p.y, p.x );
    }


    /*
    ==
    */

    Boxed_Region::Boxed_Region( int32_t width, int32_t height, int32_t x, int32_t y ) noexcept: Region( width, height, x, y ), _clip{ newwin( height - 2, width - 2, y + 1, x + 1 ) }
    {
        draw_border();
    }

    Boxed_Region::Boxed_Region( Boxed_Region &&mv ) noexcept: Region{ std::move( mv ) }, _clip{ mv._clip } 
    {
        mv._clip = nullptr;
    }

    Boxed_Region::~Boxed_Region()
    {
        if ( _clip )
        {
            clear_border();
            delwin( _clip );
        }
    }

    Boxed_Region::operator WINDOW*() const noexcept
    {
        return _clip;
    }

    void Boxed_Region::clear_border() const noexcept
    {
        wborder( _win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' );
        wrefresh( _win );
        wrefresh( _clip );
    }

    void Boxed_Region::draw_border( chtype verch, chtype horch ) const noexcept
    {
        ::box( _win, verch, horch );
    }

    int Boxed_Region::present() const noexcept
    {
        draw_border();
        Region::present();
        return wrefresh( _clip );
    }

    int32_t Boxed_Region::width() const noexcept
    {
        return static_cast<int32_t>( getmaxx( _clip ) );
    }

    int32_t Boxed_Region::height() const noexcept
    {
        return static_cast<int32_t>( getmaxy( _clip ) );
    }
}