/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "nc.h"
#include "common.h"

namespace tgl
{
    NCurses::NCurses() noexcept
    {    
        if( !NCurses::_initialized )
        {
            initscr();
            cbreak();
            keypad( stdscr, TRUE );
            start_color();
            noecho();
            nodelay(stdscr, TRUE);  // make getch() non-blocking
            raw();  // don't wait for return on input
            curs_set( 0 ); // hide input cursor

            use_default_colors();
            assume_default_colors( -1, -1 );

            ::refresh();

            NCurses::_initialized = true;
            _deinit = true;
        }
    }

    NCurses::~NCurses()
    {
        if( _deinit )
        {
            endwin();
        }
    }

    NCurses::NCurses( NCurses &&n ) noexcept: _deinit{ n._deinit }
    {
        n._deinit = false;
    }

    NCurses& NCurses::operator =( NCurses &&n ) noexcept
    {
        _deinit = n._deinit;
        n._deinit = false;

        return *this;
    }

    auto NCurses::refresh() const noexcept
    {
        return ::refresh();
    }

    int32_t NCurses::height() const noexcept { return getmaxy( stdscr ); }
    int32_t NCurses::width() const noexcept { return getmaxx( stdscr ); }
}