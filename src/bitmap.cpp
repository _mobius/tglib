/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include "bitmap.h"

namespace tgl
{
    /*
    ===== BITMAP =====
    */
    Bitmap::Bitmap( int32_t width, int32_t height, const Color_Pair &fill, const Color_Pair &transparent ): 
        transparent{ transparent }, 
        _width{ width }, _height{ height }, 
        _color_data( _width * _height, fill )
    {}

    Bitmap::Bitmap( Bitmap &&b ) noexcept: 
        _width{ b._width }, _height{ b._height }, 
        _color_data{ std::move_if_noexcept( b._color_data ) }
    {
        b._width = b._height = 0;
    }
            
    Bitmap& Bitmap::operator=( Bitmap &&b ) noexcept
    {
        _width = b._width;
        _height = b._height;

        _color_data = std::move_if_noexcept( b._color_data );

        b._width = b._height = 0;

        return *this;
    }

    void Bitmap::set_pixel( int32_t x, int32_t y, const Color_Pair &color )
    {
        get_pixel( x, y ) = color;
    }

    void Bitmap::clear_pixel( int32_t x, int32_t y )
    {
        set_pixel( x, y, {} );
    }

    Bitmap::iterator Bitmap::get_pixel( int32_t x, int32_t y )
    {
        assert( x >= 0 && x <= width() && "Bitmap_Iterator out of bounds on X" );
        assert( y >= 0 && y <= height() && "Bitmap_Iterator out of bounds on Y" );

        return begin() + (y * _width + x);
    }

    Bitmap::const_iterator Bitmap::get_pixel( int32_t x, int32_t y ) const
    { 
        assert( x >= 0 && x <= width() && "Bitmap_Iterator out of bounds on X" );
        assert( y >= 0 && y <= height() && "Bitmap_Iterator out of bounds on Y" );

        return cbegin() + (y * _width + x);
    }

    Bitmap::iterator Bitmap::get_pixel( const Vec2 &point )
    {
        return get_pixel( point.x, point.y );
    }

    Bitmap::const_iterator Bitmap::get_pixel( const Vec2 &point ) const
    {
        return get_pixel( point.x, point.y );
    }

    bool Bitmap::point_within( int32_t x, int32_t y ) const noexcept
    {
        return x >= 0 && x < width() && y >= 0 && y < height();
    }

    bool Bitmap::point_within( const Vec2 &point ) const noexcept
    {
        return point.x >= 0 && point.x < width() && point.y >= 0 && point.y < height();
    }

    std::pair<Bitmap::iterator, bool> Bitmap::pixel_at( int32_t x, int32_t y ) noexcept
    {
        if( !point_within( x, y ) ) { return { end(), false }; }

        return { get_pixel( x, y ), true };
    }

    std::pair<Bitmap::const_iterator, bool> Bitmap::pixel_at( int32_t x, int32_t y ) const noexcept
    {
        if( !point_within( x, y ) ) { return { cend(), false }; }

        return { get_pixel( x, y ), true };
    }
}