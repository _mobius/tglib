/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <thread>
#include <array>
#include <algorithm>

#include "tglib.h"

/*
 *  Draw random pixels
 */

int main()
{
    using namespace tgl;

    constexpr int surface_width = 20;
    constexpr int surface_height = 20;

    NCurses nc{};
    Keyboard kb{}; 

    Surface pixels{ surface_width, surface_height, surface_width * 2 + 1, surface_height + 1 };
    Surface boxes{ surface_width, surface_height, surface_width * 2 + 1, 0 };   // x2: pixel width is set to 2
    Surface fills{ surface_width, surface_height, 0, surface_height + 1 };

    std::array<Color, 9> colors{
        Color::none,
        Color::black,
        Color::red,
        Color::green,
        Color::yellow,
        Color::blue,
        Color::magenta,
        Color::cyan,
        Color::white
    };

    Random<int32_t> x_rand{ 0, surface_width - 1 };
    Random<int32_t> y_rand{ 0, surface_height - 1 };
    Random<int32_t> color_rand{ 0, colors.size() - 1 };

    Bitmap box{{
        { { Color::green }, { Color::blue } },
        { { Color::red }, { Color::yellow } }
    }};

    constexpr std::chrono::milliseconds delay{ 16 };
    while( true )
    {
        kb.update();
        if( kb.pressed( 'q' ) ) { break; }

        auto color = color_rand();

        // draw a pixel
        pixels.set_pixel( x_rand(), y_rand(), colors[ color ] );

        if( color == 6 )
        {  
            constexpr int max_x = (surface_width * 2) - 4;
            constexpr int max_y = surface_height - 1;
            print( std::min( x_rand() * 2, max_x ), std::min( y_rand(), max_y ), { colors[ color_rand() ], colors[ color_rand() ] }, "Text" );
        }

        // blit
        if( color == 4 )
            blit( boxes, box, { x_rand(), y_rand() } );

        // fill area
        if( color == 2 )
        {
            fill( fills, { Vec2{ x_rand(), y_rand() }, color_rand(), color_rand() }, colors[ color_rand() ] );
        }

        // draw, present changes
        pixels.present(); 
        boxes.present();
        fills.present();

        std::this_thread::sleep_for( delay );
    }

    return 0;
}