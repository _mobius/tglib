/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include "tglib.h"

/*
 *  Changing "pixel" dimenions
 *
 */

int main()
{
    using namespace tgl;

    NCurses nc{};   // sets up ncurses, cleans up on destruction
    Input_Map input{
        { 'q', "quit" },
        { KEY_LEFT, "move_left" },
        { KEY_RIGHT, "move_right" },
        { KEY_UP, "move_up" },
        { KEY_DOWN, "move_down" }
    }; 

            // surface with default pixel size (2, 1)
    Surface draw_surf{ 10, 10, 0, 0, Color::none };
        
            // double the render size to (4,2). A 10x10 surface will be (4*10, 2*10) characters in size.
    Surface draw_zoom{ 10, 10, draw_surf.full_width(), 0, Color::none, 4, 2 };

    // create a color pair (fore/back) (must create NCurses first)
    Color_Pair red{ Color::red };
    Color_Pair grn{ Color::green };
    Color_Pair blk{ Color::black };

    Bitmap box{{
        { red, blk, red },
        { blk, grn, blk },
        { red, blk, red }
    }};

    Vec2 xy{};

    blit( draw_surf, box, { 3, 3 } ); // can draw outside of the loop if it's never moved / changed

    while( true )
    {
        input.update();
        if( input.is_active( "quit" ) ) break;

        // draw box on surface
        if( input.is_active( "move_right" ) ) ++xy.x;
        else if( input.is_active( "move_left" ) ) --xy.x;
        if( input.is_active( "move_up" ) ) --xy.y;
        else if( input.is_active( "move_down" ) ) ++xy.y;

        // clear zoom surface
        fill( draw_zoom, Color::none );

        // render draw_surf onto draw_zoom offset by xy
        blit( blt::opaque, draw_zoom, draw_surf, xy );

        // draw, present changes
        draw_surf.present();
        draw_zoom.present();
    }

    return 0;
}
