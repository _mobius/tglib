/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include "tglib.h"

int main()
{
    using namespace tgl;

    NCurses nc{};
    Keyboard kb{};

    print( "Press 'q' to quit..." );
    while( true )
    {
        kb.update();
        if( kb.pressed( 'q' ) ) break;
    }

    return 0;
}