/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <algorithm>
#include <thread>

#include "tglib.h"

int main()
{
    using namespace tgl;

    NCurses nc{};
    Keyboard keyboard{};

    Surface surface{ 40, 14, 0, 0 };
    fill( surface, Color::none );

    Bitmap big{ 20, 10, Color::yellow };

    Bitmap small{ 2, 1, Color::blue };

    Vec2 big_pos{ surface.width() / 2, 2 };
    Vec2 small_pos{ big.width() / 2, big.height() - small.height() - 1 };

    Vec2 big_vel{ 1, 0 };
    Vec2 small_vel{ 1, 1 };

    const auto update_vel = []( Vec2 &pos, Vec2 &vel, auto &obj, auto &container )
    {
        if( pos.x <= 0 )
        {
            vel.x = 1;
        }
        else if ( pos.x + obj.width() >= container.width() )
        {
            vel.x = -1;
        }

        if( pos.y <= 0 )
        {
            vel.y = 1;
        }
        else if ( pos.y + obj.height() >= container.height() )
        {
            vel.y = -1;
        }
    };

    while( !keyboard.pressed( 'q' ) )
    {
        keyboard.update();

        fill( blt::opaque, big, small, small_pos, Color::yellow );
        fill( blt::opaque, surface, big, big_pos, Color::none );

        big_pos += big_vel;
        small_pos += small_vel;

        update_vel( big_pos, big_vel, big, surface );
        update_vel( small_pos, small_vel, small, big );

        blit( big, small, small_pos );
        blit( surface, big, big_pos );

        surface.present();

        std::this_thread::sleep_for( std::chrono::milliseconds{ 50 } );
    }

    return 0;
}