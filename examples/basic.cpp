/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include "tglib.h"

/*
 *  Basic Setup
 *
 */

int main()
{
    using namespace tgl;

    NCurses nc{};   // sets up ncurses, cleans up on destruction
    Input_Map input{
        { 'q', "quit" },
        { KEY_LEFT, "move_left" },
        { KEY_RIGHT, "move_right" },
        { KEY_UP, "move_up" },
        { KEY_DOWN, "move_down" }
    }; 

                        // width, height, x offset (column), y offset (row)
    Surface draw_surf{ 20, 20, 0, 0 };
    Surface surf2{ 20, 20, 40, 0 };

    // create a color pair (fore/back) (must create NCurses first)
    Color_Pair red{ Color::red };
    Color_Pair grn{ Color::green };

    // create simple "bitmap" sprite
    Bitmap box{{
        { red, red, red },
        { red, grn, red },
        { red, red, red }
    }};

    Bitmap mask{ 3, 3 };
    mask.transparent = grn;

    blit( blt::full, mask, box, { 0, 0 } );
    fill( mask, Color::cyan, red );

    Vec2 xy{};

    while( true )
    {
        input.update();
        if( input.is_active( "quit" ) ) break;

        // fill surface with a color
        fill( draw_surf, Color::none );

        // draw a pixel
        draw_surf.set_pixel( 10, 10, { Color::blue } );

        // fill masked area with magenta (ignoring transparent mask pixels)
        fill( blt::opaque, draw_surf, mask, { 9, 9 }, Color::magenta );

        // draw box on surface
        blit( blt::opaque, draw_surf, box, { 1, 1 } );
        blit( blt::transparent, draw_surf, box, { 5, 1 } );
        blit( blt::full, draw_surf, box, { 9, 1 } );
        blit( draw_surf, box, { 13, 1 }, red );

        blit( blt::opaque, draw_surf, mask, { 1, 5 } );
        blit( blt::transparent, draw_surf, mask, { 5, 5 } );
        blit( blt::full, draw_surf, mask, { 9, 5 } );
        blit( draw_surf, mask, { 13, 5 }, grn );

        if( input.is_active( "move_right" ) )
        {   ++xy.x; }
        else if( input.is_active( "move_left" ) )
        {   --xy.x; }

        if( input.is_active( "move_up" ) )
        {   --xy.y; }
        else if( input.is_active( "move_down" ) )
        {   ++xy.y; }

        // render view onto view
        View ds_view{ draw_surf };
        View s_view{ surf2 };
        fill( blt::opaque, s_view, Color::none );
        blit( blt::opaque, s_view, ds_view, xy );

        // draw, present changes
        draw_surf.present();
        surf2.present();
    }

    return 0;
}
