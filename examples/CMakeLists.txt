cmake_minimum_required(VERSION 3.13...3.16)
cmake_policy(VERSION 3.13...3.16)

project(tgl_examples VERSION 0.1
              LANGUAGES CXX)

set( PROJECTS "basic;keyboard;minimal_setup;random;pixel_size" )

foreach( PROJECT ${PROJECTS} )
    add_executable( ${PROJECT} ${PROJECT}.cpp )
    target_link_libraries( ${PROJECT} PUBLIC tglib )
    target_compile_features( ${PROJECT} PRIVATE cxx_std_17 )
endforeach( PROJECT )

