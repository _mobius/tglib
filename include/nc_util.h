/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL__UTIL_H
#define TGL__UTIL_H

#include <ncurses.h>
#include "nc_color.h"
#include "region.h"
#include "vec.h"

namespace tgl
{
    template<typename T>
    class Use{};

    /***
    * sets active color for window and unsets color on destruction
    */
    class Use_Color
    {
        public:
            Use_Color( const Color_Pair &cpair, const Region *region = nullptr ): color_pair{ cpair }, _region{ region }
            {
                if( _region )
                {
                    wattr_on( *_region, COLOR_PAIR( color_pair.id() ), nullptr );
                }
                else
                {
                    attr_on( COLOR_PAIR( color_pair.id() ), nullptr );
                    _color_off_fn = &Use_Color::_color_off;
                }
            }
            Use_Color( Color forground, Color background, const Region *region = nullptr  ): Use_Color{ { forground, background }, region } {}
            Use_Color( Color color, const Region *region = nullptr  ): Use_Color{ color, color, region } {}

            ~Use_Color()
            {
                (this->*_color_off_fn)();
            }

            Color_Pair color_pair;

        private:
            void _region_color_off()
            {
                wattr_off( *_region, COLOR_PAIR( color_pair.id() ), nullptr );
            }

            void _color_off()
            {
                attr_off( COLOR_PAIR( color_pair.id() ), nullptr );
            }

            const Region *_region{ nullptr };

            void    (Use_Color::*_color_off_fn)(){ &Use_Color::_region_color_off };
    };

    void draw( const Region &region, const Vec2 &p, const Color_Pair &color, char chr = ' ' );
}

#endif