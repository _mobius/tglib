/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL_COLOR_H
#define TGL_COLOR_H

#include "nc.h"

namespace tgl
{
    enum class Color : int16_t
    {
        none = -1,
        black = COLOR_BLACK,
        red = COLOR_RED,
        green = COLOR_GREEN,
        yellow = COLOR_YELLOW,
        blue = COLOR_BLUE,
        magenta = COLOR_MAGENTA,
        cyan = COLOR_CYAN,
        white = COLOR_WHITE,
    };

    /**
    * foreground / background color pair
    *    Don't create any before ncurses has been initialized.
    */
    class Color_Pair
    {
        public:
            using id_t = int32_t;

            Color_Pair( Color fg, Color bg ) noexcept;
            Color_Pair( Color clr = Color::none ) noexcept : Color_Pair{ clr, clr } {}

            Color_Pair( Color_Pair &&cpair ) noexcept = default;
            Color_Pair( const Color_Pair &cpair ) noexcept = default;

            ~Color_Pair() = default;

            Color_Pair& operator =( Color_Pair &&c ) noexcept;
            Color_Pair& operator =( const Color_Pair &c ) noexcept;

            explicit operator int() const noexcept { return _id; }

            id_t id() const noexcept { return _id; }
        private:
            id_t _id{ 0 };
    };

    bool operator ==( const Color_Pair &rhs, const Color_Pair &lhs ) noexcept;
    bool operator !=( const Color_Pair &rhs, const Color_Pair &lhs ) noexcept;
}

#endif