/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef TGL_COMMON_H
#define TGL_COMMON_H

#include <cassert>
#include <chrono>

namespace tgl
{
    using clock = std::chrono::steady_clock;
    using time_point = clock::time_point;

    template<bool Condition, typename TType, typename FType>
    struct enable_if_else
    {};

    template<typename TType, typename FType>
    struct enable_if_else<true, TType, FType>
    {
        using type = TType;
    };

    template<typename TType, typename FType>
    struct enable_if_else<false, TType, FType>
    {
        using type = FType;
    };

    template<bool Condition, typename TType, typename FType>
    using enable_if_else_t = typename enable_if_else<Condition, TType, FType>::type;
}

#endif