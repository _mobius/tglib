/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef TGL_PRINT_H
#define TGL_PRINT_H

#include "nc.h"
#include "region.h"
#include "nc_color.h"

namespace tgl
{
    template<typename ...Args>
    void print( const std::string &msg, Args &&...args )
    {
        printw( msg.c_str(), std::forward<Args>( args )... );
    }

    template<typename ...Args>
    void print( const Region &region, const std::string &msg, Args &&...args )
    {
        wprintw( region, msg.c_str(), std::forward<Args>( args )... );
    }

    /*
        Color, default position
    */
    template<typename ...Args>
    void print( const Color_Pair &colors, const std::string &msg, Args &&...args )
    {
        Use_Color u{ colors };
        printw( msg.c_str(), std::forward<Args>( args )... );
    }

    template<typename ...Args>
    void print( const Region &region, const Color_Pair &colors, const std::string &msg, Args &&...args )
    {
        Use_Color u{ colors };
        wprintw( region, msg.c_str(), std::forward<Args>( args )... );
    }


    /*
        Positioned / Moved (Vec2)
    */
    template<typename ...Args>
    void print( const Vec2 &position, const Color_Pair &colors, const std::string &msg, Args &&...args )
    {
        Use_Color u{ colors };
        mvprintw( position.y, position.x, msg.c_str(), std::forward<Args>( args )... );
    }

    template<typename ...Args>
    void print( const Vec2 &position, const std::string &msg, Args &&...args )
    {
        mvprintw( position.y, position.x, msg.c_str(), std::forward<Args>( args )... );
    }

    template<typename ...Args>
    void print( const Region &region, const Vec2 &position, const std::string &msg, Args &&...args )
    {
        wmvprintw( region, position.y, position.x, msg.c_str(), std::forward<Args>( args )... );
    }
    

    template<typename ...Args>
    void print( const Region &region, const Vec2 &position, const Color_Pair &colors, const std::string &msg, Args &&...args )
    {
        Use_Color u{ colors };
        wmvprintw( region, position.y, position.x, msg.c_str(), std::forward<Args>( args )... );
    }

    /*
        Positioned / Moved (x, y)
    */
    template<typename ...Args>
    void print( int32_t x, int32_t y, const Color_Pair &colors, const std::string &msg, Args &&...args )
    {
        Use_Color u{ colors };
        mvprintw( y, x, msg.c_str(), std::forward<Args>( args )... );
    }

    template<typename ...Args>
    void print( int32_t x, int32_t y, const std::string &msg, Args &&...args )
    {
        mvprintw( y, x, msg.c_str(), std::forward<Args>( args )... );
    }

    template<typename ...Args>
    void print( const Region &region, int32_t x, int32_t y, const std::string &msg, Args &&...args )
    {
        wmvprintw( region, y, x, msg.c_str(), std::forward<Args>( args )... );
    }
    

    template<typename ...Args>
    void print( const Region &region, int32_t x, int32_t y, const Color_Pair &colors, const std::string &msg, Args &&...args )
    {
        Use_Color u{ colors };
        wmvprintw( region, y, x, msg.c_str(), std::forward<Args>( args )... );
    }
}

#endif