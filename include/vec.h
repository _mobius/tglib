/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef TGL_VEC_H
#define TGL_VEC_H

#include <cstdint>

namespace tgl
{
    struct Vec2
    {
        constexpr Vec2( int32_t xi = 0, int32_t yi = 0 ) noexcept: x{xi}, y{yi} {}

        Vec2& operator+=( const Vec2 &v ) noexcept;
        Vec2& operator-=( const Vec2 &v ) noexcept;

        union
        {
            int32_t x;
            int32_t width;
        };

        union
        {
            int32_t y;
            int32_t height;
        };
    };

    bool operator==( const Vec2 &v1, const Vec2 &v2 ) noexcept;
    bool operator!=( const Vec2 &v1, const Vec2 &v2 ) noexcept;
    Vec2 operator+( const Vec2 &v1, const Vec2 &v2 ) noexcept;
    Vec2 operator-( const Vec2 &v1, const Vec2 &v2 ) noexcept;
    Vec2 operator-( const Vec2 &v ) noexcept;
    Vec2 operator*( const Vec2 &v, int m ) noexcept;


    struct Rect
    {
        constexpr Rect( int32_t x, int32_t y, int32_t w, int32_t h ) noexcept: x{ x }, y{ y }, width{ w }, height{ h } {}
        constexpr Rect( const Vec2 &xy, int32_t w, int32_t h ) noexcept: Rect{ xy.x, xy.y, w, h } {}
        constexpr Rect( const Vec2 &xy, const Vec2 &wh ) noexcept: Rect{ xy, wh.width, wh.height } {}
        constexpr Rect( int32_t x, int32_t y, const Vec2 &wh ) noexcept: Rect{ x, y, wh.width, wh.height } {}
        
        int32_t x;
        int32_t y;

        int32_t width;
        int32_t height;
    };
}

#endif