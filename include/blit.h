/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL_BLIT_H
#define TGL_BLIT_H

#include <type_traits>

#include "blit_def.h"
#include "view.h"

namespace tgl
{
    /*
    *  Src & Dest must be View convertable
    *
    */
    template<typename Src, typename Dest, typename Test, typename ...Proc>
    auto blit( Dest &&dest, const Src &source, const Vec2 &offset, Test &&test, Proc &&...proc ) -> std::enable_if_t<std::is_invocable_v<Test, decltype(*source.cbegin()), decltype(*dest.begin()), const Vec2 &, const Vec2 &>, void>
    {
        auto [dest_view, src_view] = clip_pair( std::forward<Dest>( dest ), source, offset );

        assert( dest_view.width() <= dest.width() && "Blit dest width malformed" );
        assert( dest_view.height() <= dest.height() && "Blit dest height malformed" );
        assert( src_view.width() <= source.width() && "Blit src width malformed" );
        assert( src_view.height() <= source.height() && "Blit src height malformed" );

        auto dest_itr = dest_view.begin();
        auto dest_end = dest_view.end();

        auto src_itr = src_view.cbegin();
        auto src_end = src_view.cend();

        for( ; dest_itr != dest_end && src_itr != src_end; ++dest_itr, ++src_itr )
        {
            if ( test( *dest_itr, *src_itr, dest_itr.position(), src_itr.position() ) )
            {
                (proc( *dest_itr, *src_itr, dest_itr.position(), src_itr.position() ),...);
            }
        }
    }

    // copy bitmap; skip bitmap pixels that are `ignore` color
    template<typename Src, typename Dest, typename ...Proc>
    void blit( Dest &&dest, const Src &source, const Vec2 &offset, const Color_Pair &ignore, Proc &&...proc )
    {
        blit( std::forward<Dest>( dest ), source, offset, [ignore]( auto &, auto &source, auto, auto ) { return source != ignore; }, std::forward<Proc>( proc )... );
    }

    template<typename Src, typename Dest>
    void blit( Dest &&dest, const Src &source, const Vec2 &offset, const Color_Pair &ignore )
    {
        blit( std::forward<Dest>( dest ), source, offset, ignore, blt::copy );
    }


    // copy all
    template<typename Src, typename Dest, typename ...Proc>
    void blit( blt::Full, Dest &&dest, const Src &source, const Vec2 &offset, Proc &&...proc )
    {
        blit( std::forward<Dest>( dest ), source, offset, blt::always_true, std::forward<Proc>( proc )... );
    }

    template<typename Src, typename Dest>
    void blit( blt::Full full, Dest &&dest, const Src &source, const Vec2 &offset )
    {
        blit( full, std::forward<Dest>( dest ), source, offset, blt::copy );
    }

    // copy source to dest; skip pixels that are transparent (requires Src::transparent to exist)
    template<typename Src, typename Dest, typename ...Proc>
    void blit( blt::Opaque, Dest &&dest, const Src &source, const Vec2 &offset, Proc &&...proc )
    {
        blit( std::forward<Dest>( dest ), source, offset, source.transparent, std::forward<Proc>( proc )... );
    }

    template<typename Src, typename Dest>
    void blit( blt::Opaque opaque, Dest &&dest, const Src &source, const Vec2 &offset )
    {
        blit( opaque, std::forward<Dest>( dest ), source, offset, blt::copy );
    }


    // copy source to dest; skip pixels that are opaque (requires Src::transparent to exist)
    template<typename Src, typename Dest, typename ...Proc>
    void blit( blt::Transparent, Dest &&dest, const Src &source, const Vec2 &offset, Proc &&...proc )
    {
        blit( std::forward<Dest>( dest ), source, offset, [cmp = source.transparent]( auto &, auto &src, auto, auto ) { return src == cmp; }, std::forward<Proc>( proc )... );
    }

    template<typename Src, typename Dest>
    void blit( blt::Transparent trans, Dest &&dest, const Src &source, const Vec2 &offset )
    {
        blit( trans, std::forward<Dest>( dest ), source, offset, blt::copy );
    }

    // some default mode
    template<typename Src, typename Dest>
    void blit( Dest &&dest, const Src &source, const Vec2 &offset )
    {
        blit( blt::opaque, std::forward<Dest>( dest ), source, offset, blt::copy );
    }
}

#endif