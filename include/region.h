/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL_REGION_H
#define TGL_REGION_H

#include <string>
#include <ncurses.h>

#include "nc.h"
#include "vec.h"

namespace tgl
{

    /**
    * wrapper for ncurses Window
    */
    class Region
    {
        public:
            Region() noexcept = default;
            Region( int32_t width, int32_t height, int32_t x, int32_t y ) noexcept;
            Region( int32_t width, int32_t height ) noexcept;
            Region( const NCurses &nc, int32_t pad = 0 ) noexcept;

            Region( Region &&region ) noexcept;
            Region( Region &region ) = delete;

            virtual ~Region();

            virtual operator WINDOW*() const noexcept;

            virtual int present() const noexcept;

            virtual int32_t width() const noexcept;
            virtual int32_t height() const noexcept;

            // change size, position
            void modify( int32_t width, int32_t height, int32_t x, int32_t y ) noexcept;
            void move( const Vec2 &p ) noexcept;

            template<typename... Args>
            auto print( const Vec2 &mv, const std::string &str, const Args &&...args ) noexcept
            {
                mvwprintw( _win, mv.y, mv.x, str.c_str(), args... );
            }

        protected:
            WINDOW* _win{ nullptr };
    };


    /**
    * wrapper for ncurses window that has a border
    */
    class Boxed_Region : public Region
    {
        public:
            Boxed_Region( int32_t width, int32_t height, int32_t x, int32_t y ) noexcept;
            Boxed_Region( int32_t width, int32_t height ) noexcept: Boxed_Region( width, height, 0, 0 ) {}
            Boxed_Region( const NCurses &nc, int32_t pad = 0 ) noexcept: Boxed_Region( nc.width() - (pad*2), nc.height() - (pad*2), pad, pad ) {}

            Boxed_Region( Boxed_Region &&mv ) noexcept;

            ~Boxed_Region();

            operator WINDOW*() const noexcept override;

            void clear_border() const noexcept;

            void draw_border( chtype verch = 0, chtype horch = 0 ) const noexcept;

            int present() const noexcept override;

            int32_t width() const noexcept override;
            int32_t height() const noexcept override;

            template<typename... Args>
            auto print( const Vec2 &mv, const std::string &str, const Args &&...args ) noexcept
            {
                mvwprintw( _clip, mv.y, mv.x, str.c_str(), args... );
            }

        private:
            WINDOW* _clip{ nullptr }; // draw 'box' on Region::_win, draw everything else to this win (to avoid 1,1 offset stuff)
    };
}

#endif 