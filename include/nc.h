/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL_NC_H
#define TGL_NC_H

#include <ncurses.h>
#include <cstdint>

namespace tgl
{
    class NCurses
    {
        public:
            NCurses() noexcept;
            ~NCurses();

            NCurses( NCurses && ) noexcept;
            NCurses( const NCurses & ) = delete;

            NCurses& operator =( NCurses && ) noexcept;
            NCurses& operator =( const NCurses & ) = delete;

            auto refresh() const noexcept;

            int32_t height() const noexcept;
            int32_t width() const noexcept;

            static bool is_initialized() noexcept { return NCurses::_initialized; }
        public:
        private:
            bool _deinit{ false };
            inline static bool  _initialized = false;
    };
}

#endif