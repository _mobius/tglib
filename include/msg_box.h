/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL_MSG_BOX_H
#define TGL_MSG_BOX_H

#include "vec.h"
#include "region.h"

namespace tgl
{
    class Msg_Box
    {
        public:
            enum align { left, center, right };

            Msg_Box( const std::string &msg, const Vec2 &xy, align align = left ):
                region{ static_cast<int32_t>( msg.size() + 2 ), 3, xy.x, xy.y }, message{ msg }
            {
                calc_alignment( align );
            }

            Msg_Box( int32_t width, const std::string &msg, const Vec2 &xy, align align = left ): region{ width + 2, 3, xy.x, xy.y },  message{ msg }
            {
                calc_alignment( align );
            }

            void present()
            {
                region.print( pos, message );
                region.present();
            }

            void clear()
            {
                region.print( { 0, 0 }, std::string( " ", message.size() ) );
                region.present();
                region.clear_border();
            }

            void set_text( const std::string &str )
            {
                message = str;
            }

            void set_text( const std::string &str, align ment )
            {
                message = str;
                calc_alignment( ment );
            }

            void calc_alignment( align ment )
            {
                auto pad = 0;
                if( ment == center )
                {
                    pad = (region.width() / 2) - (message.length() / 2);
                }
                else if ( ment == right )
                {
                    pad = region.width() - message.length();
                }
                pos = Vec2{ pad, 0 };
            }

            Boxed_Region region;
            Vec2         pos;
            std::string message;
    };
}

#endif // NC_MSG_BOX_H
