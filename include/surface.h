/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef TGL_SURFACE_H
#define TGL_SURFACE_H

#include <vector>
#include <cassert>

#include "nc.h"
#include "nc_color.h"
#include "region.h"
#include "bitmap.h"

#include "surface_itr.h"

namespace tgl
{
    class Surface
    {
        public:
            using region_collection_t = std::vector<Region>;

            using iterator       = Surface_Iterator<Surface, region_collection_t::iterator, Bitmap::iterator>;
            using const_iterator = Surface_Iterator<const Surface, region_collection_t::const_iterator, Bitmap::const_iterator>;

            iterator begin() { return { this, _draw_regions.begin(), _color_data.begin() }; }
            iterator end()   { return { this, _draw_regions.end(), _color_data.end() }; }

            const_iterator cbegin() const { return { this, _draw_regions.cbegin(), _color_data.cbegin() }; }
            const_iterator cend()   const { return { this, _draw_regions.cend(), _color_data.cend() }; }

            Surface( int32_t width, int32_t height, int32_t x, int32_t y, uint32_t pixel_width = 2, uint32_t pixel_height = 1 );
            Surface( int32_t width, int32_t height, int32_t x, int32_t y, const Color_Pair &fill, uint32_t pixel_width = 2, uint32_t pixel_height = 1 );

            bool point_within( int32_t x, int32_t y ) const noexcept;
            bool point_within( const Vec2 &point ) const noexcept;

            std::pair<iterator, bool> pixel_at( int32_t x, int32_t y ) noexcept;  // checks that x,y is valid, then gets pixel
            std::pair<const_iterator, bool> pixel_at( int32_t x, int32_t y ) const noexcept;  // checks that x,y is valid, then gets pixel

            std::pair<iterator, bool> pixel_at( const Vec2 &point ) noexcept;
            std::pair<const_iterator, bool> pixel_at( const Vec2 &point ) const noexcept;
            
            iterator get_pixel( int32_t x, int32_t y );
            iterator get_pixel( const Vec2 &p );

            const_iterator get_pixel( int32_t x, int32_t y ) const;
            const_iterator get_pixel( const Vec2 &p ) const;

            void set_pixel( int32_t x, int32_t y, const Color_Pair &color );
            void set_pixel( const Vec2 &p, const Color_Pair &color );

            // set pixels in rect defined by [ul, lr]
            void set_pixels( const Vec2 &upper_left, const Vec2 &lower_right, const Color_Pair &color );
            void set_pixels( const std::vector<Vec2> &points, const Color_Pair &color );
            
            auto width() const { return _color_data.width(); }
            auto height() const { return _color_data.height(); }

            auto full_width() const { return static_cast<int32_t>( width() * _pixel_width ); }
            auto full_height() const { return static_cast<int32_t>( height() * _pixel_height ); }

            void present();
            void mark_update( iterator itr );

            Color_Pair &transparent;
        private:
            Vec2     _position;

            uint32_t _pixel_width;
            uint32_t _pixel_height;

            bool _redraw_all{ false };

            Bitmap _color_data; // color info for each pixel


            std::vector<iterator> _dirty_pixels;
            region_collection_t   _draw_regions;   // each "pixel" is a region
    };

}

#endif