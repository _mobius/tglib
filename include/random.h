/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef TGL_RANDOM_H
#define TGL_RANDOM_H

#include <random>
#include "common.h"

namespace tgl
{
    template<typename T = int32_t, typename Generator = std::default_random_engine >
    class Random
    {
        public:
            static_assert( std::is_arithmetic_v<T>, "tgl::Random<T> requires number type." );
            using type_t = T;

            using distrib_t = enable_if_else_t< std::is_integral_v<T>, std::uniform_int_distribution<type_t>, std::uniform_real_distribution<type_t> >;

            Random( type_t low, type_t high ) : _distrib{ low, high } {}

            type_t get() 
            {
                return _distrib( _gen );
            }

            type_t operator()()
            {
                return _distrib( _gen );
            }

            Generator& generator() { return _gen; }
        private:
            std::random_device _rand_dev{};
            Generator          _gen{ std::random_device{}() };
            std::uniform_int_distribution<type_t> _distrib{};
    };
}

#endif