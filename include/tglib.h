/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef TGLIB_H
#define TGLIB_H

#include "common.h"
#include "random.h"
#include "input_map.h"
#include "nc_util.h"
#include "msg_box.h"
#include "surface.h"
#include "fill.h"
#include "print.h"

#endif