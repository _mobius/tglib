/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL_VIEW_H
#define TGL_VIEW_H

#include <utility>
#include <type_traits>

namespace tgl
{
    template<typename ViewT, typename SubItr>
    class View_Iterator
    {
        public:
            using iterator_category = std::forward_iterator_tag;
            using value_type = typename SubItr::value_type;
            using difference_type = Vec2;
            using pointer = typename SubItr::pointer;
            using reference = typename SubItr::reference;

            View_Iterator( ViewT *view, const Vec2 &point ): _pos{ point }, _itr{ view->viewable().get_pixel( point ) }, _view{ view } {}

            View_Iterator() = default;
            View_Iterator( View_Iterator && ) noexcept = default;
            View_Iterator( const View_Iterator & ) noexcept = default;

            ~View_Iterator() = default;

            View_Iterator& operator =( View_Iterator && ) noexcept = default;
            View_Iterator& operator =( const View_Iterator & ) noexcept = default;

            View_Iterator& operator ++() 
            {
                if( _pos != _view->lower_right() )
                {
                    ++_pos.x;
                    ++_itr;

                    if( _pos.x >= _view->lower_right().x )
                    {
                        if( _view->lower_right().y - _pos.y > 1 )
                        {
                            _pos.x = _view->upper_left().x;
                            ++_pos.y;
                            _itr += _view->viewable().width() - _view->width();
                        }
                        else
                        {
                            _pos = _view->lower_right();
                            _itr = _view->viewable().get_pixel( _pos );
                        }
                    }
                }

                return *this;
            }

            View_Iterator operator ++( int )
            {
                View_Iterator ret{ *this };
                ++(*this);

                return ret;
            }

            reference operator *() { return *_itr; }
            pointer   operator->() { return &(*_itr); }

            bool operator ==( const View_Iterator &itr ) const noexcept
            {
                return _view == itr._view && itr._pos == _pos;
            }

            bool operator !=( const View_Iterator &itr ) const noexcept
            {
                return !(*this == itr);
            }

            const Vec2& position() const { return _pos; }
        private:

            Vec2   _pos{};
            SubItr _itr{};
            ViewT  *_view{ nullptr };
    };

    class View_Base {};
    template<typename T>
    class View : public View_Base
    {
        public:
            using type_t   = T;
            using iterator = View_Iterator<View, typename type_t::iterator>;
            using const_iterator = View_Iterator<const View, typename type_t::const_iterator>;

            iterator begin() { return { this, _upper_left }; }
            iterator end()   { return { this, _lower_right };  }

            const_iterator cbegin() const { return { this, _upper_left }; }
            const_iterator cend()   const { return { this, _lower_right };  }

            View( type_t &vo, int32_t ulx, int32_t uly, int32_t lrx, int32_t lry ) noexcept: View_Base{},
                transparent{ vo.transparent },
                _upper_left{ std::min( vo.width(), std::max( ulx, 0 ) ), std::min( vo.height(), std::max( uly, 0 ) ) },
                _lower_right{ std::min( vo.width(), std::max( _upper_left.x, lrx ) ), std::min( vo.height(), std::max( _upper_left.y, lry ) ) },
                _viewable{ &vo }
            {
                fix_points( ulx, uly, lrx, lry, { vo.width(), vo.height() } );
            }

            View( type_t &vo, const Vec2 &upper_left, const Vec2 &lower_right ) noexcept: View{ vo, upper_left.x, upper_left.y, lower_right.x, lower_right.y } {}
            View( type_t &vo, const Rect &rect ) noexcept: View{ vo, rect.x, rect.y, std::max( rect.x, rect.x + rect.width ), std::max( rect.y, rect.y + rect.height ) } {}
            View( type_t &vo, const Vec2 &upper_left ) noexcept: View{ vo, { upper_left.x, upper_left.y, vo.width(), vo.height() } } {}
            View( type_t &vo ) noexcept: View_Base{}, transparent{ vo.transparent }, _upper_left{ 0, 0 }, _lower_right{ vo.width(), vo.height() }, _viewable{ &vo } {}

            View( View &&v ) noexcept = default;
            View( const View &v ) noexcept = default;

            ~View() = default;

            View& operator =( View && ) noexcept = default;
            View& operator =( const View & ) noexcept = default;

            void fix_points( int32_t ulx, int32_t uly, int32_t lrx, int32_t lry, const Vec2 &max_lr, const Vec2 &min_ul = {} ) noexcept
            {
                if( (ulx < 0 && -ulx > max_lr.x) || (uly < 0 && -uly > max_lr.y) )
                { _upper_left = min_ul; }

                if( _upper_left.x >= max_lr.x || _upper_left.y >= max_lr.y )
                { _upper_left = _lower_right = max_lr; }
                else if( lrx <= _upper_left.x || lry <= _upper_left.y )
                { _lower_right = _upper_left; }
            }

            // adjust points from current size (shrink only)
            void resize( int32_t ulx, int32_t uly, int32_t lrx, int32_t lry ) noexcept
            {
                const auto ul_ = _upper_left;
                const auto lr_ = _lower_right;

                _upper_left = { std::min( lr_.x, std::max( ulx, ul_.x ) ), std::min( lr_.y, std::max( uly, ul_.y ) ) };
                _lower_right = { std::min( lr_.x, std::max( _upper_left.x, lrx ) ), std::min( lr_.y, std::max( _upper_left.y, lry ) ) };

                fix_points( ulx, uly, lrx, lry, lr_, ul_ );
            }
            
            void resize( const Vec2 &ul ) noexcept
            {
                resize( _upper_left.x + ul.x, _upper_left.y + ul.y, _lower_right.x, _lower_right.y );
            }

            void resize( const Vec2 &ul, const Vec2 &lr ) noexcept
            {
                // vec args would be given assuming [0,0] is upper left, so adjust for underlying viewable offset.
                //      if view( [10,10], [20,20] ) (10 width/height) -> resize: [1,1][8, 10] -> pass on [11,11][18,20]
                resize( _upper_left.x + ul.x, _upper_left.y + ul.y, _upper_left.x + lr.x, _upper_left.y + lr.y );
            }

            void resize( const Rect &rect )
            {
                // adjust rect offsets to the existing viewable offset
                const auto x = _upper_left.x + rect.x;
                const auto y = _upper_left.y + rect.y;

                resize( x, y,
                        x + std::min( width(), rect.width ),
                        y + std::min( height(), rect.height ) );
            }

            type_t& viewable() { return *_viewable; }
            const type_t& viewable() const { return *_viewable; }

            auto width() const noexcept { return _lower_right.x - _upper_left.x; }
            auto height() const noexcept { return _lower_right.y - _upper_left.y; }

            const Vec2& upper_left() const noexcept { return _upper_left; }
            const Vec2& lower_right() const noexcept { return _lower_right; }

            const Vec2& position() const noexcept { return _upper_left; }

            const Color_Pair& transparent;
        private:
            Vec2    _upper_left{};
            Vec2    _lower_right{};

            type_t  *_viewable;
    };


    /*
    *   Make a View from a View with arguments for size
    */
    template<typename T, std::enable_if_t<std::is_base_of_v<View_Base, std::remove_reference_t<std::remove_pointer_t<std::remove_cv_t<T>>>>, int> = 0, typename ...Args>
    auto make_view( T &&vo, Args &&...args )
    {
        if constexpr( sizeof...( args ) )
        {
            // arguments given, keep old view's dimensions then resize.
            View v{ std::forward<T>( vo ) };
            v.resize( std::forward<Args>( args )... );

            return v;
        }

        return View{ std::forward<T>( vo ) };
    }

    // make view from non-view (bitmap/surface)
    template<typename T, std::enable_if_t<!std::is_base_of_v<View_Base, std::remove_reference_t<std::remove_pointer_t<std::remove_cv_t<T>>>>, int> = 0, typename ...Args>
    auto make_view( T &&vo, Args &&...args )
    {
        return View{ std::forward<T>( vo ), std::forward<Args>( args )... };
    }

    /*
    *  create a clipped / cutout view of outer starting at offset using inner
    *  
    */
    template<typename T, typename U>
    auto inner_clip( T &&outer, const U &inner, const Vec2 &offset = { 0, 0 } )
    {
        return make_view( std::forward<T>( outer ), Rect{ offset, inner.width(), inner.height() } );
    }

    /*
    *   create a clipped view of `inner` when moved to `offset` within `outer` so that it doesn't go outside of outer's boundry
    *
    */
    template<typename T, typename U>
    auto outer_clip( T &&inner, const Vec2 &offset, const U &outer )
    {
        return make_view( std::forward<T>( inner ), Rect{ offset.x < 0 ? -offset.x : 0, offset.y < 0 ? -offset.y : 0, outer.width() - offset.x, outer.height() - offset.y } );
    }

    // create matching view clips that overlap
    template<typename T, typename U>
    auto clip_pair( T &&bottom, U &&top, const Vec2 &offset = { 0, 0 } )
    {
        return std::make_pair(
            inner_clip( std::forward<T>( bottom ), std::forward<U>( top ), offset ),
            outer_clip( std::forward<U>( top ), offset, std::forward<T>( bottom ) )
        );
    }
}

#endif