/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL_BITMAP_H
#define TGL_BITMAP_H

#include <vector>
#include <cassert>

#include "nc_color.h"
#include "vec.h"

#include "bitmap_itr.h"

#include "blit_def.h"

namespace tgl
{
    class Bitmap
    {
        public:
            using color_data_t = std::vector<Color_Pair>;

            using iterator       = Bitmap_Iterator<Bitmap, color_data_t::iterator>;
            using const_iterator = Bitmap_Iterator<const Bitmap, color_data_t::const_iterator>;

            iterator begin() { return { this, std::begin( _color_data ) }; }
            iterator end() { return { this, std::end( _color_data ) }; }

            const_iterator cbegin() const { return { this, std::cbegin( _color_data ) }; }
            const_iterator cend()   const { return { this, std::cend( _color_data ) }; }

            Bitmap( int32_t width = 0, int32_t height = 0, const Color_Pair &fill = {}, const Color_Pair &transparent = {} );

            template<size_t W, size_t H>
            Bitmap( const Color_Pair (&color)[H][W], const Color_Pair &transparent = {}) : Bitmap{ W, H, transparent, transparent }
            {
                auto p = get_pixel( 0, 0 );

                for( auto &row : color )
                {
                    for( auto &cp : row )
                    {
                        p = cp;
                        ++p;
                    }
                }
            }

            Bitmap( Bitmap &&b ) noexcept;
            Bitmap( const Bitmap & ) = default;

            ~Bitmap() = default;

            Bitmap& operator=( Bitmap &&b ) noexcept;
            Bitmap& operator=( const Bitmap &b ) = default;

            template<size_t W, size_t H>
            Bitmap& operator =( const Color_Pair (&color)[H][W] )
            {
                _color_data.reserve( W * H );
                auto p = _color_data.begin();

                for( auto &row : color )
                {
                    for( auto &cp : row )
                    {
                        *p = cp;
                        ++p;
                    }
                }

                return *this;
            }

            void set_pixel( int32_t x, int32_t y, const Color_Pair &color );

            void clear_pixel( int32_t x, int32_t y );                // set pixel to default color

            iterator       get_pixel( int32_t x, int32_t y );
            const_iterator get_pixel( int32_t x, int32_t y ) const;

            iterator       get_pixel( const Vec2 &point );
            const_iterator get_pixel( const Vec2 &point ) const;

            bool point_within( int32_t x, int32_t y ) const noexcept;
            bool point_within( const Vec2 &point ) const noexcept;

            std::pair<iterator, bool> pixel_at( int32_t x, int32_t y ) noexcept;  // checks that x,y is valid, then gets pixel
            std::pair<const_iterator, bool> pixel_at( int32_t x, int32_t y ) const noexcept;  // checks that x,y is valid, then gets pixel

            int32_t width() const noexcept { return _width; }
            int32_t height() const noexcept { return _height; }

            std::vector<Color_Pair>& data() { return _color_data; }
            const std::vector<Color_Pair>& data() const { return _color_data; }

            Color_Pair                transparent{};
        private:
            int32_t     _width, 
                        _height;

            color_data_t _color_data;
    };

}

#endif