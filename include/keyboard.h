/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL_KEYBOARD_H
#define TGL_KEYBOARD_H

#include <unordered_map>

#include "common.h"

namespace tgl
{
    class Keyboard
    {

        public:
                // set this to account for press-to-repeat delay
            inline static std::chrono::milliseconds repeat_delay{ 0 };

            enum class Key_State_Flag : uint8_t { none, just = 1, up = 2, down = 4 };
            enum class Key_State : uint8_t { 
                                pressed = static_cast<uint8_t>( Key_State_Flag::down ),
                                just_pressed = static_cast<uint8_t>( Key_State_Flag::down ) | static_cast<uint8_t>( Key_State_Flag::just ),
                                released = static_cast<uint8_t>( Key_State_Flag::up ),
                                just_released = static_cast<uint8_t>( Key_State_Flag::up ) | static_cast<uint8_t>( Key_State_Flag::just )
                            };

            class Key_Info
            {
                public:
                    inline static std::chrono::milliseconds step_wait{ 15 };

                    explicit Key_Info( int k ) noexcept;

                    Key_Info( Key_Info && ) noexcept = default;
                    Key_Info( const Key_Info & ) noexcept = default;

                    ~Key_Info() = default;

                    bool is_down() const noexcept;
                    bool pressed() const noexcept;
                    bool just_pressed() const noexcept;

                    bool is_up() const noexcept;
                    bool released() const noexcept;
                    bool just_released() const noexcept;

                    std::chrono::milliseconds state_duration() const noexcept;

                    bool is_state( Key_State state ) const noexcept;

                    // moves state from "just_*something*" to *something* when held (step_wait)
                    void step_state() noexcept;

                    void press() noexcept;
                    void release() noexcept;

                    const int   key;
                    Key_State   state{ Key_State::released };
                    time_point  last_changed{ clock::now() - std::chrono::milliseconds{ 1000 } };
                    bool        checked{ false };

                private:
                    void _set_state( Key_State_Flag direction ) noexcept;
            };

            void update();

            Key_Info& operator []( int key_id );

            // convenience methods (keyboard[key].pressed() -> keyboard.pressed( key ))
            bool pressed( int key_id );
            bool just_pressed( int key_id );

            bool released( int key_id );
            bool just_released( int key_id );

            bool is_state( int key_id, Key_State );

            std::unordered_map<int, Key_Info>     keys;
    };
}
#endif