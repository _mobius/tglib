/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef TGL_BITMAP_ITR_H
#define TGL_BITMAP_ITR_H

#include "nc_color.h"
#include "vec.h"

namespace tgl
{
    template<typename Bmp, typename Itr>
    class Bitmap_Iterator
    {
        public:
            using iterator_category = std::forward_iterator_tag;

            using value_type = typename Itr::value_type;
            using difference_type = Vec2;
            using pointer = typename Itr::pointer;
            using reference = typename Itr::reference;

            Bitmap_Iterator() noexcept = default;
            Bitmap_Iterator( Bitmap_Iterator && ) noexcept = default;
            Bitmap_Iterator( const Bitmap_Iterator & ) noexcept = default;
            Bitmap_Iterator( Bmp *bmp, Itr itr ) noexcept : _bitmap{ bmp }, _bm_itr{ itr } {}

            ~Bitmap_Iterator() = default;

            Bitmap_Iterator& operator =( Bitmap_Iterator && ) noexcept = default;
            Bitmap_Iterator& operator =( const Bitmap_Iterator & ) noexcept = default;

            Bitmap_Iterator& operator ++()
            {
                ++_bm_itr;

                return *this;
            }

            Bitmap_Iterator& operator ++( int )
            {
                auto r{ *this };

                ++_bm_itr;

                return r;
            }

            Bitmap_Iterator& operator --()
            {
                --_bm_itr;

                return *this;
            }

            Bitmap_Iterator& operator --( int )
            {
                auto r{ *this };

                --_bm_itr;

                return r;
            }

            bool operator ==( const Bitmap_Iterator &itr ) const
            {
                return _bm_itr == itr._bm_itr;
            }

            bool operator !=( const Bitmap_Iterator &itr ) const
            {
                return _bm_itr != itr._bm_itr;
            }

            reference operator *() { return *_bm_itr; }
            pointer   operator->() { return &(*_bm_itr); }

            Bitmap_Iterator& operator +=( const Vec2 &point )
            {
                _bm_itr += (point.y * _bitmap->width() + point.x);

                return *this;
            }

            Bitmap_Iterator& operator -=( const Vec2 &point )
            {
                _bm_itr -= (point.y * _bitmap->width() + point.x);

                return *this;
            }

            Bitmap_Iterator& operator +=( int32_t i )
            {
                _bm_itr += i;

                return *this;
            }

            Bitmap_Iterator& operator -=( int32_t i )
            {
                _bm_itr -= i;

                return *this;
            }

            Bitmap_Iterator& operator =( const Color_Pair &color )
            {
                *_bm_itr = color;

                return *this;
            }

            Bitmap_Iterator& operator =( const Color &color )
            {
                *_bm_itr = { color };

                return *this;
            }

        private:
            Bmp *_bitmap{ nullptr };
            Itr  _bm_itr{};

    };

    template<typename B, typename I>
    bool operator ==( const Bitmap_Iterator<B,I> &pixel, const Color_Pair &color )
    {
        return *pixel == color;
    }

    template<typename B, typename I>
    bool operator !=( const Bitmap_Iterator<B,I> &pixel, const Color_Pair &color )
    {
        return *pixel != color;
    }

    template<typename B, typename I>
    Bitmap_Iterator<B,I> operator +( Bitmap_Iterator<B,I> pixel, const Vec2 &point )
    {
        pixel += point;

        return pixel;
    }

    template<typename B, typename I>
    Bitmap_Iterator<B,I> operator -( Bitmap_Iterator<B,I> pixel, const Vec2 &point )
    {
        pixel -= point;

        return pixel;
    }
}

#endif