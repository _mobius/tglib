/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef TGL_SURFACE_ITR_H
#define TGL_SURFACE_ITR_H

#include "nc_color.h"
#include "vec.h"

namespace tgl
{
    template<typename Srfc, typename SItr, typename BmpItr>
    class Surface_Iterator
    {
        public:
            using iterator_category = std::forward_iterator_tag;

            class Pixel
            {
                public:
                    Pixel( Srfc *surface, Surface_Iterator *itr ) noexcept: _surface{ surface }, _itr{ itr } {}
                    Pixel( Pixel &&p ) noexcept = default; //: _surface{ p._surface }, _itr{ p._itr } {}
                    Pixel( const Pixel &p ) noexcept = default; //: _surface{ p._surface }, _itr{ p._itr } {}

                    ~Pixel() = default;

                    Pixel& operator =( Pixel &&p ) noexcept
                    {   
                        _surface = p._surface;
                        _itr = p._itr;
                        return *this;
                    }
                    Pixel& operator =( const Pixel &p ) noexcept
                    {
                        _surface = p._surface;
                        _itr = p._itr;
                        return *this;
                    }

                    typename SItr::pointer operator ->() { return &(*(_itr->_region_itr)); }
                    //typename BmpItr::value_type operator *() { return *_itr._bmp_itr; }

                    using region_t = typename SItr::value_type;
                    operator region_t&() { return *(_itr->_region_itr); }

                    using color_t = typename BmpItr::value_type;
                    operator color_t() { return *(_itr->_bmp_itr); }

                    Pixel& operator =( const Color_Pair &color )
                    {
                        _itr->_bmp_itr = color;
                        _surface->mark_update( *_itr );

                        return *this;
                    }

                    Pixel& operator =( const Surface_Iterator &pair )
                    {
                        *(_itr->_bmp_itr) = *(pair._bmp_itr);
                        _surface->mark_update( *_itr );

                        return *this;
                    }

                private:
                    Srfc   *_surface{ nullptr };
                    Surface_Iterator *_itr;
            };

            friend Pixel;
            friend Srfc;

            using value_type = Pixel;
            using difference_type = Vec2;
            using pointer = value_type*;
            using reference = value_type&;

            Surface_Iterator() = default;

            Surface_Iterator( Srfc *surface, SItr sitr, BmpItr bitr ) noexcept : _surface{ surface }, _region_itr{ sitr }, _bmp_itr{ bitr }, _pixel{ surface, this } {}
            Surface_Iterator( Surface_Iterator &&itr ) noexcept : _surface{ itr._surface }, _region_itr{ itr._region_itr }, _bmp_itr{ itr._bmp_itr }, _pixel{ itr._surface, this } {}
            Surface_Iterator( const Surface_Iterator &itr ) noexcept : _surface{ itr._surface }, _region_itr{ itr._region_itr }, _bmp_itr{ itr._bmp_itr }, _pixel{ itr._surface, this } {}

            Surface_Iterator& operator =( Surface_Iterator &&itr ) noexcept
            {
                _surface = itr._surface;
                _region_itr = itr._region_itr;
                _bmp_itr = itr._bmp_itr;
                _pixel = { _surface, this };

                return *this;
            }

            Surface_Iterator& operator =( const Surface_Iterator &itr ) noexcept
            {
                _surface = itr._surface;
                _region_itr = itr._region_itr;
                _bmp_itr = itr._bmp_itr;
                _pixel = { _surface, this };

                return *this;
            }

            Surface_Iterator& operator ++()
            {
                ++_region_itr;
                ++_bmp_itr;

                return *this;
            }

            Surface_Iterator& operator ++( int )
            {
                auto r{ *this };

                ++_region_itr;
                ++_bmp_itr;

                return r;
            }

            Surface_Iterator& operator --()
            {
                --_region_itr;
                --_bmp_itr;

                return *this;
            }

            Surface_Iterator& operator --( int )
            {
                auto r{ *this };

                --_region_itr;
                --_bmp_itr;

                return r;
            }

            bool operator ==( const Surface_Iterator &itr ) const
            {
                return _region_itr == itr._region_itr;
            }

            bool operator !=( const Surface_Iterator &itr ) const
            {
                return _region_itr != itr._region_itr;
            }

            reference operator *() { return _pixel; }
            typename SItr::pointer operator->() { return _region_itr.operator->(); }

            Surface_Iterator& operator +=( const Vec2 &point )
            {
                const auto jump = (point.y * _surface->width() + point.x);
                _region_itr += jump;
                _bmp_itr += jump;

                return *this;
            }

            Surface_Iterator& operator -=( const Vec2 &point )
            {
                const auto jump = (point.y * _surface->width() + point.x);
                _region_itr -= jump;
                _bmp_itr -= jump;

                return *this;
            }

            Surface_Iterator& operator +=( int32_t i )
            {
                _region_itr += i;
                _bmp_itr += i;

                return *this;
            }

            Surface_Iterator& operator -=( int32_t i )
            {
                _region_itr -= i;
                _bmp_itr -= i;

                return *this;
            }

            Surface_Iterator& operator =( const Color_Pair &color )
            {
                _surface->mark_update( *this );
                _bmp_itr = color;

                return *this;
            }

            Surface_Iterator& operator =( const Color &color )
            {
                _surface->mark_update( *this );
                _bmp_itr = color;

                return *this;
            }

            operator Color_Pair() { return *_bmp_itr; }
            operator Region&() { return *_region_itr; }

            Color_Pair& color() { return *_bmp_itr; }
            Region&     region() { return *_region_itr; }
        private:
            Srfc *_surface{ nullptr };

            SItr   _region_itr{};
            BmpItr _bmp_itr{};

            Pixel  _pixel;

    };

    template<typename S, typename I, typename B>
    bool operator ==( const Surface_Iterator<S,I,B> &itr, const Color_Pair &color )
    {
        return *itr == color;
    }

    template<typename S, typename I, typename B>
    bool operator !=( const Surface_Iterator<S,I,B> &itr, const Color_Pair &color )
    {
        return *itr != color;
    }

    template<typename S, typename I, typename B>
    Surface_Iterator<S,I,B> operator +( Surface_Iterator<S,I,B> itr, const Vec2 &point )
    {
        itr += point;

        return itr;
    }

    template<typename S, typename I, typename B>
    Surface_Iterator<S,I,B> operator -( Surface_Iterator<S,I,B> itr, const Vec2 &point )
    {
        itr -= point;

        return itr;
    }

}

#endif