/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL_BLIT_DEF_H
#define TGL_BLIT_DEF_H


namespace tgl
{
    namespace blt
    {
        struct Transparent {};  // skips source transparent pixels
        struct Opaque {};       // copies everything, even trasparent pixels
        struct Full {};

        constexpr Opaque opaque{};
        constexpr Transparent transparent{};
        constexpr Full full{};

        constexpr auto always_true = []( auto&, auto&, auto, auto ) noexcept { return true; };
        constexpr auto copy = []( auto &dest, auto &source, auto, auto ) { dest = source; };
    }
}

#endif