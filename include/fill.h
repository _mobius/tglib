/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL_FILL_H
#define TGL_FILL_H

#include "blit.h"

namespace tgl
{
    // fill / replace any color that matches `match`
    template<typename T>
    void fill( T &&dest, const Color_Pair &fill, const Color_Pair &match )
    {
        for( auto &p : dest )
        {
            if( p == match )
            {
                p = fill;
            }
        }
    }

    // fill all opaque pixels with color
    template<typename T>
    void fill( blt::Opaque, T &&dest, const Color_Pair &color )
    {
        for( auto &p : dest )
        {
            if( p != dest.transparent )
            {
                p = color;
            }
        }
    }
    
    // fill all transparent pixels with color
    template<typename T>
    void fill( blt::Transparent, T &&dest, const Color_Pair &color )
    {
        fill( std::forward<T>( dest ), color, dest.transparent );
    }

    // fill entire surface/bitmap with color
    template<typename T>
    void fill( T &&dest, const Color_Pair &color )
    {
        for( auto &p : dest )
        {
            p = color;
        }
    }

    // fill defined section
    template<typename T>
    void fill( T &&dest, const Rect &area, const Color_Pair &color )
    {
        fill( make_view( std::forward<T>( dest ), area ), color );
    }

    template<typename T>
    void fill( T &&dest, const Vec2 &up_left, const Vec2 &low_right, const Color_Pair &color )
    {
        fill( make_view( std::forward<T>( dest ), up_left, low_right ), color );
    }

    // fill in the masked area with a color (using blt:: modes)
    template<typename Mode, typename T, typename Mask>
    void fill( Mode mode, T &&dest, const Mask &mask, const Vec2 &mask_offset, const Color_Pair &fill_color )
    {
        blit( mode, std::forward<T>( dest ), mask, mask_offset, [fill_color]( auto &dest, auto &, auto, auto ) { dest = fill_color; } );
    }

    template<typename T, typename Mask>
    void fill( T &&dest, const Mask &mask, const Vec2 &mask_offset, const Color_Pair &fill_color )
    {
        fill( blt::opaque, std::forward<T>( dest ), mask, mask_offset, [fill_color]( auto &dest, auto &, auto, auto ) { dest = fill_color; } );
    }
}

#endif