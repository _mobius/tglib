/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef TGL_INPUT_MAP_H
#define TGL_INPUT_MAP_H

#include <map>
#include <set>
#include <initializer_list>

#include "keyboard.h"

namespace tgl
{
    class Input_Map
    {
        public:
            using action_name_t = std::string;
            using map_pair_t = std::pair<int,tgl::Keyboard::Key_State>;

            Input_Map() noexcept = default;

            Input_Map( std::initializer_list<std::pair<int, action_name_t>> pressed_map ) noexcept
            {
                for( auto &[key, action] : pressed_map )
                {
                    map_key( key, action );
                }
            }

            Input_Map( std::initializer_list<std::pair<map_pair_t, action_name_t> > maps ) noexcept
            {
                for( auto &[mp, action] : maps )
                {
                    map_key( mp.first, action, mp.second );
                }
            }

            Input_Map( Input_Map && ) noexcept = default;
            Input_Map( const Input_Map & ) = default;

            ~Input_Map() = default;

            Input_Map& operator =( Input_Map && ) noexcept = default;
            Input_Map& operator =( const Input_Map & ) = default;

            bool map_key( int key_id, const action_name_t &name, tgl::Keyboard::Key_State state = tgl::Keyboard::Key_State::pressed  )
            {
                auto [itr, inserted] = input_map.emplace( std::make_pair( key_id, state ), name );

                return inserted;
            }

            void unmap_key( int key_id, tgl::Keyboard::Key_State state )
            {
                if( auto itr = input_map.find( std::make_pair( key_id, state ) ); itr != input_map.end() )
                {
                    input_map.erase( itr );
                }
            }

            void update() 
            {
                keyboard.update();
                active_actions.clear();

                for( auto &[ks, action] : input_map )
                {
                    auto &[key_id, key_state] = ks;
                    if( keyboard.is_state( key_id, key_state ) )
                    {
                        active_actions.emplace( action );
                    }
                }
            }

            bool is_active( const action_name_t &name ) const
            {
                return active_actions.find( name ) != active_actions.cend();
            }

            void clear_action( const action_name_t &name )
            {
                if( auto action = active_actions.find( name ); action != active_actions.end() )
                {
                    active_actions.erase( action );
                }
            }

            tgl::Keyboard keyboard{};
            
                // <key, key_state> can be bound to an action
            std::map<map_pair_t, action_name_t> input_map{};

            std::set<action_name_t> active_actions{};
    };
}

#endif